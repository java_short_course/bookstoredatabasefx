package sample.controllers;

import com.jfoenix.controls.JFXTextField;
import com.sun.istack.internal.NotNull;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import sample.model.dao.AuthorDAO;
import sample.model.dao.ConnectionDb;
import sample.model.dto.Author;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

public class Controller  implements Initializable {



    AuthorDAO authorDAO = new AuthorDAO();
    Author selectedAuthor;

    @FXML
    public JFXTextField txtPhone;

    @FXML
    private JFXTextField txtAddress;

    @FXML
    private DatePicker dpkDob;

    @FXML
    private ComboBox<String> cboGender;

    @FXML
    private JFXTextField txtEmail;

    @FXML
    private JFXTextField txtLastName;

    @FXML
    private JFXTextField txtFirstName;

    @FXML
    private TableView<Author> tableView;
    @FXML
    private TableColumn<Author, String> colFirstName;
    @FXML
    private TableColumn<Author, String> colLastName;

    @FXML
    void onSave(ActionEvent event) {
//        System.out.println(authorDAO.getAllAuthors());

        Author author = new Author();
        if (txtFirstName.validate())
        author.setFirstName(txtFirstName.getText().trim().toString());
        author.setLastName(txtLastName.getText().trim().toString());

        author.setGender(cboGender.getSelectionModel().getSelectedItem().charAt(0));
        author.setDob(Date.valueOf(dpkDob.getValue()));
        author.setEmail(txtEmail.getText().trim().toString());
        author.setPhoneNumber(txtPhone.getText().trim().toString());
        clearData();
        //System.out.println(authorDAO.addAuthor(author));


}

    void clearData(){
        txtFirstName.clear();
        txtLastName.clear();
        txtAddress.clear();
        txtEmail.clear();
        txtPhone.clear();
        cboGender.getSelectionModel().clearSelection();

    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cboGender.getItems().addAll("Male", "Female");
        colFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        colLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tableView.setItems(authorDAO.getAllAuthors());
    }

    public void onDelete(ActionEvent actionEvent) {
       // Author author = (Author) authorDAO.getAllAuthors();
       // authorDAO.removeArticle(author.getId());

        Author author = new Author();
        if (txtFirstName.validate())
            author.setFirstName(txtFirstName.getText().trim().toString());
        author.setLastName(txtLastName.getText().trim().toString());

        author.setGender(cboGender.getSelectionModel().getSelectedItem().charAt(0));
        author.setDob(Date.valueOf(dpkDob.getValue()));
        author.setEmail(txtEmail.getText().trim().toString());
        author.setPhoneNumber(txtPhone.getText().trim().toString());
        author.setId(selectedAuthor.getId());
        authorDAO.updateAuthor(author);
    }

    public void onMouseClicked(MouseEvent mouseEvent) {

        selectedAuthor = tableView.getSelectionModel().getSelectedItem();
        txtFirstName.setText(selectedAuthor.getFirstName());
        txtLastName.setText(selectedAuthor.getLastName());

    }
}
