package sample.model.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.model.dto.Author;

import javax.xml.transform.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorDAO {

    public ObservableList<Author> getAllAuthors() {
        ObservableList<Author> authors = FXCollections.observableArrayList();

        // Opnen Connection
        ConnectionDb.openConnection();

        // GET DATA
        String sql = "SELECT * FROM bms_authors\n" +
                "WHERE status IS TRUE;";
        try {
            PreparedStatement preparedStatement = ConnectionDb.connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            Author author;
            while (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getInt(1));
                author.setFirstName(resultSet.getString(2));
                author.setLastName(resultSet.getString(3));
                author.setGender(resultSet.getString(4).charAt(0));
                author.setDob(resultSet.getDate(5));
                author.setEmail(resultSet.getString(6));
                author.setPhoneNumber(resultSet.getString(7));
                author.setAddress(resultSet.getString(8));
                author.setImageUrl(resultSet.getString(9));
                author.setStatus(resultSet.getBoolean(10));

                // add author into authors
                authors.add(author);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        // After get data we need to close connection
        ConnectionDb.closeConnection();
        return authors;
    }

    public Boolean addAuthor(Author author) {
        System.out.println("====> " + author);
        boolean b = false;
        ConnectionDb.openConnection();
        String sql = "INSERT INTO bms_authors (first_name, last_name, gender, birth_date, email, phone_number, address, image_url)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement preparedStatement = ConnectionDb.connection.prepareStatement(sql);
            preparedStatement.setString(1, author.getFirstName());
            preparedStatement.setString(2, author.getLastName());
            preparedStatement.setString(3, author.getGender().toString());
            preparedStatement.setDate(4, author.getDob());
            preparedStatement.setString(5, author.getEmail());
            preparedStatement.setString(6, author.getPhoneNumber());
            preparedStatement.setString(7, author.getAddress());
            preparedStatement.setString(8, author.getImageUrl()                                           );

            int numberOfRowEffected = preparedStatement.executeUpdate();
            if (numberOfRowEffected > 0)
                b = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ConnectionDb.closeConnection();
        return b;
    }


    public Boolean updateAuthor(Author author) {
        boolean isUpdated = false;
        ConnectionDb.openConnection();
        String sql = "UPDATE bms_authors  SET " +
                "first_name=?, last_name=?, gender=?, birth_date=?, email=?," +
                " phone_number=?, address=?, image_url=? WHERE id =?";

        try {
            PreparedStatement preparedStatement = ConnectionDb.connection.prepareStatement(sql);
            preparedStatement.setString(1, author.getFirstName());
            preparedStatement.setString(2, author.getLastName());
            preparedStatement.setString(3, author.getGender().toString());
            preparedStatement.setDate(4, author.getDob());
            preparedStatement.setString(5, author.getEmail());
            preparedStatement.setString(6, author.getPhoneNumber());
            preparedStatement.setString(7, author.getAddress());
            preparedStatement.setString(8, author.getImageUrl());
            preparedStatement.setInt(9, author.getId());

            int numberOfRowEffected = preparedStatement.executeUpdate();
            if (numberOfRowEffected > 0)
                isUpdated = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ConnectionDb.closeConnection();
        return isUpdated;
    }


    public Boolean removeArticle(int id){
        boolean value = false;
        ConnectionDb.openConnection();
        String sql = "DELETE FROM bms_authors where id="+id;
        try {
            PreparedStatement preparedStatement = ConnectionDb.connection.prepareStatement(sql);
            int numberOfRowEffected = preparedStatement.executeUpdate();
            if (numberOfRowEffected > 0)
                value = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }


        ConnectionDb.closeConnection();
        return value;
    }
}
