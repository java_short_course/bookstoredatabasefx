package sample.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDb {
    public static Connection connection = null;

    public static void openConnection() {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        try {
            connection = DriverManager.getConnection(url, "postgres", "123");
            System.out.println("connection open");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeConnection() {
        try {
            connection.close();
            System.out.println("connection close");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
